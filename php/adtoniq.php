<?php
/* Plugin Name: Adtoniq
Plugin URI: http://www.adtoniq.com/
Description: Adtoniq Component for WordPress
Version: 1.6-dev-k
Author: David Levine for Adtoniq
Author URI: http://www.adtoniq.com/
License: GPLv2 or later
*/

//static $adtoniqServer = "https://integration.adtoniq.com/";
static $adtoniqServer = "http://52.10.221.235/";

class Adtoniq {
  public static $count = 0;
  
  public static function getStr() {
    self::$count++;
    return self::$count;
  }
}

if ( ! function_exists('write_log')) {
   function write_log ( $log )  {
      if ( is_array( $log ) || is_object( $log ) ) {
         error_log( print_r( $log, true ) );
      } else {
         error_log( $log );
      }
   }
}

function fwds_adtoniq_activation() {
//	updateCache();
}
register_activation_hook(__FILE__, 'fwds_adtoniq_activation');
function fwds_adtoniq_deactivation() {
}
register_deactivation_hook(__FILE__, 'fwds_adtoniq_deactivation');

add_action('admin_menu', 'adtoniq_menu');

function adtoniq_menu() {
	add_menu_page('Adtonic', 'Adtonic', 'administrator', 'adtoniq-settings', 'adtoniq_settings_page', 'dashicons-admin-generic');
}

function adtoniq_vars_filter( $vars ){
  $vars[] = "adtoniqNonce";
  $vars[] = "adtoniqSid";
  $vars[] = "adtoniqAction";
  $vars[] = "adtoniqServer";
  return $vars;
}
add_filter( 'query_vars', 'adtoniq_vars_filter' );

function RegisterPage() {
	$email = get_option('email');
	$name = get_option('name');
	$password = get_option('password');
	$password2 = get_option('password2');
	$fqdn = get_option('fqdn');
	$description = get_option('description');
	$error = "";
	global $adtoniqServer;

	if (strlen($email) > 0) {
		if (strlen($password) == 0)
			$error .= "Missing password. ";
		if (strlen($password2) == 0)
			$error .= "Missing second password. ";
		if (strlen($name) == 0)
			$error .= "Missing your name. ";
		if (strlen($password) > 0) {
			if (strlen($password) < 8)
				$error .= "Password must be at least 8 characters. ";
			if ($password != $password2)
				$error .= "Passwords do not match. ";
		}
		if (strlen($fqdn) == 0)
			$error .= "Missing Fully Qualified Domain Name. ";

		if (strlen($error) == 0) {
			$url = $adtoniqServer . 'api/v1';
			$data = array(
			  'email' => $email,
			  'operation' => "register",
			  'name' => $name,
			  'password' => $password,
			  'fqdn' => $fqdn,
			  'description' => $description
			  );
			$response = post($url, $data);
			if (substr($response, 0, 7) == "success") {
				$state = "emailwait";
				update_option('state', $state, true);
			} else {
				if (strlen($response) == 0)
					$error .= "Error registering with Adtoniq. ";
				else
					$error .= $response;
			}
		}
	}
	if ($state == "emailwait")
		EmailWaitPage();
	else if (strlen($state) == 0) {
?>
<div class="wrap">
<img src='//d2t7a3zbo166a9.cloudfront.net/images/adtoniq-logo1.png' style='height:100px;'>
<h2>Adtoniq Settings</h2>
<p>Adtoniq helps you analyze and communicate with your ad blocked audience, recover lost revenue from ad blocked users, and protect the integrity of your web site from being broken by ad blockers.
You need an Adtoniq account to use this plugin. Get a free account by filling out the form below and clicking Save Changes. Your free account is limited to 100,000 page views per month.</p>
		<?php if (strlen($error) > 0) { ?>
<p style="color:red;"><?php echo $error; ?></p>
		<?php } ?>


	<form method="post" action="options.php">
		<?php settings_fields( 'adtoniq-settings-group' ); ?>
		<?php do_settings_sections( 'adtoniq-settings-group' ); ?>
		<table class="form-table">
			<tr valign="top">
			<th scope="row">Your email address</th>
			<td><input type="email" name="email" style="width:20em;" value="<?php echo esc_attr( $email ); ?>" /></td>
			</tr>

			<tr valign="top">
			<th scope="row">Your name</th>
			<td><input type="text" name="name" style="width:20em;" value="<?php echo esc_attr( $name ); ?>" /></td>
			</tr>

			<tr valign="top">
			<th scope="row">Password</th>
			<td><input type="password" name="password" style="width:20em;" value="<?php echo esc_attr( $password ); ?>" /></td>
			</tr>

			<tr valign="top">
			<th scope="row">Type password again</th>
			<td><input type="password" name="password2" style="width:20em;" value="<?php echo esc_attr( $password2 ); ?>" /></td>
			</tr>

			<tr valign="top">
			<th scope="row">Fully Qualified Domain Name</th>
			<td><input type="text" name="fqdn" placeholder="www.mywebsite.com" style="width:20em;" value="<?php echo esc_attr( $fqdn ); ?>" /></td>
			</tr>

			<tr valign="top">
			<th scope="row">Description of web site</th>
			<td><input type="text" name="description" style="width:20em;"  value="<?php echo esc_attr( $description ); ?>"/></td>
			</tr>

			<tr valign="top">
			<th scope="row">Terms & Conditions</th>
			<td>
				<input type="checkbox" id="agreeToTOS" name="tosGroup"/>
				<label for="agreeToTOS"><span></span>
				I agree to the <a href="//d2t7a3zbo166a9.cloudfront.net/pdf/AdtonIQ%20terms%20of%20use%20and%20conditions.pdf" target="_blank">Terms Of Service</a>
			</td>
			</tr>
		</table>
		<?php submit_button(); ?>
	</form>
</div>
<?php 
  }
}

function checkForEmailConfirmation() {
	$email = get_option('email');
	$name = get_option('name');
//	$password = get_option('password');
//	$password2 = get_option('password2');
	$fqdn = get_option('fqdn');
//	$description = get_option('description');
	global $adtoniqServer;

	$url = $adtoniqServer . 'api/v1';
	$data = array(
	  'operation' => "checkEmailConfirmation",
	  'email' => $email,
	  'name' => $name,
	  'fqdn' => $fqdn
	  );
	$apiKey = post($url, $data);
	if (strlen($apiKey) > 0) {
		update_option('api_key', $apiKey, true);
		update_option('state', 'registered', true);
	}
	
	return $apiKey;
}

function EmailWaitPage() {
	$apiKey = checkForEmailConfirmation();
	if (strlen($apiKey) == 0) {
?>
<div class="wrap">
<img src='//d2t7a3zbo166a9.cloudfront.net/images/adtoniq-logo1.png' style='height:100px;'>
<h2>Adtoniq Settings</h2>
<p>Check your email for your Adtoniq invitation. To protect the security of your account, you must accept this invitation to activate your account.
After you accept your invitation, click the Check For Email Activation button or refresh this page to continue.<p>
<input type="button" class="button" value="Check For Email Activation" onclick='location.reload();'>
<p>To reset your Adtoniq WordPress plugin, cancel your registration and start the registration step again,
click the Reset plugin and start again button below.</p>

	<form method="post" action="options.php">
		<?php settings_fields( 'adtoniq-settings-group' ); ?>
		<?php do_settings_sections( 'adtoniq-settings-group' ); ?>
		<input type="hidden" name="state" style="width:20em;"  value=""/>
		<?php submit_button(); ?>
	</form>

</div>
<script>
document.getElementById('submit').value = 'Reset plugin and start again';
</script>
<?php 
	} else
		RegisteredPage();
}

function RegisteredPage() {
	$apiKey = get_option('api_key');
	$fqdn = get_option('fqdn');
	global $adtoniqServer;
?>
<div class="wrap">
<img src='//d2t7a3zbo166a9.cloudfront.net/images/adtoniq-logo1.png' style='height:100px;'>
<h2>Adtoniq Settings V1.6-dev-k</h2>
<p>Your Adtoniq account is active and connected. You are currently using the free tier of service which gives you up to 100,000 page views per month for free.<br><br>
Monthly quota used: 0, remaining: 100K.<br> 
</p>

	<form method="post" action="<?php echo $adtoniqServer; ?>adtoniqDesigner.jsp" target="_blank">
		<p>Start Adtoniq Designer by clicking the button below.</p>
		<input type="hidden" name="operation" value="designer"/>
		<input type="hidden" name="apiKey" value="<?php echo $apiKey; ?>"/>
		<input type="hidden" name="fqdn" value="<?php echo $fqdn; ?>"/>
		<input type="submit" name="designer" style="width:20em;" value="Start Adtoniq Designer">
	</form>
	
<p>To reset your Adtoniq WordPress plugin to its original state, click the Reset plugin button below.</p>

	<form method="post" action="options.php">
		<?php settings_fields( 'adtoniq-settings-group' ); ?>
		<?php do_settings_sections( 'adtoniq-settings-group' ); ?>
		<input type="hidden" name="state" value=""/>
		<?php submit_button(); ?>
	</form>

</div>
<script>
document.getElementById('submit').value = 'Reset plugin';
</script>

</div>

<?php 
}

function adtoniq_settings_page() {
  $state = get_option('state');
  
  if (strlen($state) == 0)
    RegisterPage();
  else if ($state == "emailwait")
    EmailWaitPage();
  else if ($state == "registered")
    RegisteredPage();
}

add_action( 'wp_head', 'adtoniq_head_injection' );

function adtoniq_head_injection() {
  echo updateCache();
}

$adtoniqSidCookieName = 'adtoniq_sid';
static $sid = '';
static $lastSignin = '';

function set_user_cookie() {
	if (isset($_POST["adtoniq_designer"]))
		$adtoniqDesigner = $_POST["adtoniq_designer"];
	else
		$adtoniqDesigner = "";
	$api_key = get_option('api_key');
	$fqdn = get_option('fqdn');
	$action = isset($_POST['adtoniqAction']) ?	$_POST['adtoniqAction'] : '';
	
	global $sid;
	global $lastSignin;
	global $adtoniqServer;
	
	$sid = '';
	$lastSignin = '';
	$url = $adtoniqServer . 'controller.jsp';

	write_log('=======');
	write_log('v20');
	write_log('action is ' . $action);
	
	if ($action == 'signout') {
		$cookie_name = 'adtoniq_sid';
		unset($_COOKIE[$cookie_name]);
		// empty value and expiration one hour before
		$res = setcookie($cookie_name, '', time() - 3600, COOKIEPATH, COOKIE_DOMAIN);
		write_log('Cleared ' . $cookie_name);
	} else if ($action == 'signin') {
		$nonce = isset($_POST['adtoniqNonce']) ? $_POST['adtoniqNonce'] : '';
		write_log('resiginin and nonce is ' . $nonce);
		$data = array(
		  'operation'   => 'resignIn',
		  'nonce'   => $nonce,
		  'fqdn' => $fqdn,
		  'apiKey' => $api_key
		  );
		$sid = post($url, $data);
		write_log('Got sid ' . $sid);
		if (strlen($sid) > 0) {
			setcookie('adtoniq_sid', $sid, time()+3600, COOKIEPATH, COOKIE_DOMAIN);
			setcookie('adtoniq_lastSignin', time(), time() + (3600 * 24 * 30), COOKIEPATH, COOKIE_DOMAIN);
		}
	} else {
		if (strlen($adtoniqDesigner) > 0 && $adtoniqDesigner == $api_key) {
			$data = array(
			  'operation'   => 'signIn',
			  'fqdn'   => $fqdn,
			  'apiKey' => $api_key
			  );
			$sid = post($url, $data);
			if (strlen($sid) > 0) {
				setcookie('adtoniq_sid', $sid, time()+3600, COOKIEPATH, COOKIE_DOMAIN);
				setcookie('adtoniq_lastSignin', time(), time() + (3600 * 24 * 30), COOKIEPATH, COOKIE_DOMAIN);
			}
			write_log('signin and apiKey is ' . $api_key);
		} else {
			$sid = isset($_COOKIE['adtoniq_sid']) ? $_COOKIE['adtoniq_sid'] : '';
			$lastSignin = array_key_exists('adtoniq_lastSignin', $_COOKIE) ? $_COOKIE['adtoniq_lastSignin'] : '';
			write_log('sid is is ' . $sid);
		}
	}

}
add_action( 'init', 'set_user_cookie');

function updateCache() {
	$injectStr = get_option('head_injection', '');
	$fqdn = get_option('fqdn');
	$api_key = get_option('api_key');
	$nonce = get_query_var('adtoniqNonce', '');
	if (isset($_POST["adtoniq_designer"]))
		$adtoniqDesigner = $_POST["adtoniq_designer"];
	else
		$adtoniqDesigner = "";
	$adtoniqSid = 'adtoniq_sid';

	$extraStr = '';
	global $sid;
	global $adtoniqServer;
	
	if (strlen($injectStr) == 0 || strlen($nonce) > 0) {
//	  write_log('nonce = ' . $nonce);
	  $email = get_option('email');
	 
	  $url = get_query_var('adtoniqServer', $adtoniqServer . 'api/v1');
	  $data = array('operation' => "update", 'userName' => $email, 'apiKey' => $api_key, 'fqdn' => $fqdn, 'nonce' => $nonce);
	  $response = post($url, $data);

	  if (strlen($response) > 0)
		$injectStr = $extraStr . $response;
	  update_option('head_injection', $injectStr, true); 
	}

	if (strlen($sid) > 0) {
		$url = $adtoniqServer . 'websitesHeadInject.jsp';
		$data = array(
		  'sessionId'   => $sid
		  );
		$injectStr = '<!-- Adtoniq Designer -->' . post($url, $data);
		write_log('Just did inject designer');
	} else if (isset($_COOKIE['adtoniq_lastSignin'])) {
		$extraStr =
			"
			<script>
			var adtoniqURL = '" . $fqdn . "';
			function messageListener(event){
				if ( event.origin !== '" . $adtoniqServer . "' )
					return;
				var data = event.data;
				if (typeof(data) === 'string'){
					try {
						data = JSON.parse(data.trim()); 
					}
					catch (e) {
						;
					}
				}
				if (data && data.action) {
					if (data.action == 'adtoniq-signin') {
						var h = document.getElementById('adtoniqNonce');
						h.value = data.n;
						document.adtoniq_signin.submit();
					}
				}
			}
			if (window.addEventListener){
				  addEventListener('message', messageListener, false)
				} else {
				  attachEvent('onmessage', messageListener)
				}
			</script>
			<style>
				#adtoniq_resignin_img {
					-webkit-transition: all 0.15s ease;
					opacity: 0.25;
				}
				#adtoniq_resignin_img:hover {
					opacity: 1;
				}
			</style>
			<form method='post' action='' name='adtoniq_signin'>
				<input type='hidden' name='adtoniqAction' value='signin'/>
				<input type='hidden' name='adtoniqNonce' id='adtoniqNonce' value=''/>
			</form>
			
			<a href='javascript:void()' style='display:inline-block;height:32px;width:32px;position:fixed;left:32px;right:32px;z-index:99999999999999999999999999999999'
			onclick='adtoniq.floatIframe(\"" . $adtoniqServer . "adtoniqResignin.jsp\", 600, 350)'>
				<img id='adtoniq_resignin_img' src='//d2t7a3zbo166a9.cloudfront.net/images/logos/SWiconRGB.png' >
			</a>";
//			write_log('extraStr length: ' . strlen($extraStr));
//			write_log('Just injected re-signin code: ' . $extraStr);
	}
	return $extraStr . $injectStr;
}

function post($url, $data) {
  // use key 'http' even if you send the request to http://...
  $options = array(
	'http' => array(
		'header'  => "Content-type: application/x-www-form-urlencoded",
		'method'  => 'POST',
		'content' => http_build_query($data)
	)
  );
  $context  = stream_context_create($options);
  $response = trim(file_get_contents($url, false, $context));
  return $response;
}

add_action( 'admin_init', 'adtoniq_settings' );

function adtoniq_settings() {
	register_setting( 'adtoniq-settings-group', 'email' );
	register_setting( 'adtoniq-settings-group', 'name' );
	register_setting( 'adtoniq-settings-group', 'api_key' );
	register_setting( 'adtoniq-settings-group', 'password' );
	register_setting( 'adtoniq-settings-group', 'password2' );
	register_setting( 'adtoniq-settings-group', 'fqdn' );
	register_setting( 'adtoniq-settings-group', 'description' );
	register_setting( 'adtoniq-settings-group', 'head_injection' );
	register_setting( 'adtoniq-settings-group', 'state' );
}