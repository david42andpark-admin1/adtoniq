# README #

This repository contains the source code for the client libraries required to integrate your web server with Adtoniq. You should
choose the library that matches your content management system or web server implementation. See the wiki for more information.

Current implementations include:

* Java Server Pages (JSP) / Java
* WordPress / PHP

### What is this repository for? ###

* Adtoniq client library
* Version 1.0

### How do I get set up? ###

* Summary of set up: For Java servers, add the Java class to your web server and modify your HTML as required
* Summary of set up: For WordPress servers, add the PHP plugin to your plugins folder and activate
* Configuration: Specify your account, api key, and fqdn
* Dependencies: None
* Database configuration: None
* How to run tests: See if the JSP file loads and shows a message to ad blocked users
* Deployment instructions: Deploy in Java-based web server

### Contribution guidelines ###

* Writing tests: TBD
* Code review: TBD
* Other guidelines: TBD

### Who do I talk to? ###

* Repo owner or admin: david@adtoniq.com
* Other community or team contact: gary@streamwize.com