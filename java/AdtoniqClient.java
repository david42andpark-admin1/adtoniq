package com.adtoniq;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Hashtable;

import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class AdtoniqClient {
	final static String HOST = "integration.adtoniq.com";
	final static String PATH = "/api/v1";
	final static int POLL_INTERVAL = 60;	// Seconds
	static boolean running = false;
	static AdtoniqConnection connection = null;
	static String scriptTag;
	boolean obfuscate;

	String fqdn;
	String userName;
	String apiKey;

	String variableName;
	
	/** Connect to the Adtoniq server. After calling this, this instance will periodically
	 * poll the Adtoniq server for updates.
	 * @param fqdn The fully qualified domain name of the publisher web site
	 * @param userName The publisher username
	 * @param apiKey The publisher api key
	 */
	public AdtoniqClient(String fqdn, String userName, String apiKey) {
		this(fqdn, userName, apiKey, true);
	}
	
	/** Connect to the Adtoniq server. After calling this, this instance will periodically
	 * poll the Adtoniq server for updates.
	 * @param fqdn The fully qualified domain name of the publisher web site
	 * @param userName The publisher username
	 * @param apiKey The publisher api key
	 * @param obfuscate Set to true to generate obfuscated JavaScript
	 */
	public AdtoniqClient(String fqdn, String userName, String apiKey, boolean obfuscate) {
		this.fqdn = fqdn;
		this.userName = userName;
		this.apiKey = apiKey;
		this.obfuscate = obfuscate;
		
		if (connection == null)
			connection = new AdtoniqConnection();
	}
	
	/** Gets the script tag that should be injected into the head portion of the page
	 * @return Returns the string containing the entire script tag to be injected
	 */
	public String getScriptTag() {
		return scriptTag == null ? "" : scriptTag;
	}
	
	/** Implements the polling pattern, getting an updated script tag once
	 * per iteration.
	 * @author david
	 *
	 */
	class AdtoniqConnection {
		private Thread thread;
		
		public AdtoniqConnection() {
			updateScriptTag();
			thread = new Thread() {
				public void run() {
					thread.setName("Adtoniq Client");
					while (running) {
						updateScriptTag();
						try {
							Thread.sleep(1000 * POLL_INTERVAL);
						} catch (InterruptedException e) {
							connection = null;
							running = false;
							return;
						}
					}
				}

			};
			
			running = true;
			thread.start();
		}
		
		public void updateScriptTag() {
			scriptTag = AdtoniqClient.this.excutePost("http://" + HOST + PATH + "/settings", params());
		}
		
		public void stop() {
			thread.interrupt();
		}
		
		@SuppressWarnings("unused")
		private void parseResult(String json) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				@SuppressWarnings("unchecked")
				HashMap<String, String> content = mapper.readValue(json, HashMap.class);
				scriptTag = content.get("scriptTag");
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
	}
	
	protected void finalize() {
		connection.stop();
	}
	
	private String params() {
		return new QueryArgBuilder()
			.add("userName", userName)
			.add("apiKey", apiKey)
			.add("fqdn", fqdn)
			.add("obfuscate", obfuscate ? "1" : "0")
			
			
			.toString();
	}
	
	String excutePost(String targetURL, String urlParameters) {
		URL url;
		HttpURLConnection serverConnection = null;
		try {
			// Create connection
			url = new URL(targetURL);
			serverConnection = (HttpURLConnection) url.openConnection();
			serverConnection.setRequestMethod("POST");
			serverConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

			serverConnection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
			serverConnection.setRequestProperty("Content-Language", "en-US");

			serverConnection.setUseCaches(false);
			serverConnection.setDoInput(true);
			serverConnection.setDoOutput(true);

			// Send request
			DataOutputStream wr = new DataOutputStream(serverConnection.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			// Get Response
			StringWriter writer = new StringWriter();
			IOUtils.copy(serverConnection.getInputStream(), writer);
			return writer.toString().trim();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (serverConnection != null) {
				serverConnection.disconnect();
			}
		}
	}
	
	class QueryArgBuilder {
		Hashtable<String, String> map = new Hashtable<String, String>();
		
		public QueryArgBuilder add(String name, String value) {
			map.put(name,  value);
			return this;
		}
		
		@Override
		public String toString() {
			StringBuilder str = new StringBuilder();
			String delim = "";
			for (String s : map.keySet()) {
				str.append(delim + s + "=" + map.get(s));
				delim = "&";
			}
			return str.toString();
		}
	}

}
